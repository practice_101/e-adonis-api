import { Exception } from '@adonisjs/core/build/standalone';
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UserModel from 'App/Models/User';
import HashService from './HashService';

export default class UserService {
  private hash : HashService
  constructor(){
    this.hash = new HashService();
  }

  async login(request: HttpContextContract['request']) {
    const {email,password} = request.all();
    const user = await UserModel.findOne({ email: email }).select('password');
    if(!user) throw new Exception('user not found',404);

    const verifyPassword = await this.hash.checkHash(password,user.password)

    if(!verifyPassword) throw new Exception('invalid password',400)
    // console.log(await UserModel.exists({email:request.body()['email']}));
    // console.log(request.all());
    return request.all();
  }
}



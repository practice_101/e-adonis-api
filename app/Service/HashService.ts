import * as bcrypt from "bcrypt";

export default class HashService {
  private saltRounds = 10;

  public async hasher(code: any) {
    return bcrypt.hash(code, this.saltRounds)
  }

  public async checkHash(passcode: any, hash: any) {
    return bcrypt.compare(passcode, hash);
  }
}




import { inject } from '@adonisjs/fold';
import type { HttpContextContract, } from '@ioc:Adonis/Core/HttpContext'
import UserService from 'App/Service/UserService';

@inject()
export default class UsersController {
  private userService: UserService;

  constructor(userService: UserService) {
    this.userService = userService
  }

  async login({ request, response }: HttpContextContract) {
    try {
      const data = await this.userService.login(request)
      return response.json(data);
    } catch (error) {
      return response.status(error.status).json({ message: error.message })
    }
  }
}

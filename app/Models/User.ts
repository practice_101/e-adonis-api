// import Mongoose,{ Schema } from "@ioc:Mongoose";

import mongoose, { Schema } from "mongoose";

const Mongoose  = mongoose

const UserModel = Mongoose.model('users',new Schema({
  name: {
      type: String,
  },
  email: {
      type: String,
      unique: true
  },
  password: {
      type: String,
      select: false
  },
}, { timestamps: true }));

export default UserModel;



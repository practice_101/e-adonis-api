import type { ApplicationContract } from '@ioc:Adonis/Core/Application'
import mongoose from 'mongoose'

/*
|--------------------------------------------------------------------------
| Provider
|--------------------------------------------------------------------------
|
| Your application is not ready when this file is loaded by the framework.
| Hence, the top level imports relying on the IoC container will not work.
| You must import them inside the life-cycle methods defined inside
| the provider class.
|
| @example:
|
| public async ready () {
|   const Database = this.app.container.resolveBinding('Adonis/Lucid/Database')
|   const Event = this.app.container.resolveBinding('Adonis/Core/Event')
|   Event.on('db:query', Database.prettyPrint)
| }
|
*/
export default class MongooseProvider {
  constructor(protected app: ApplicationContract) {}

  public async register() {
    // Register your own bindings
    this.app.logger.info('Init Mongoose Provider')

    await mongoose.connect('mongodb://localhost:27017/adonis_api');

    this.app.container.singleton('Mongoose',() => mongoose);

    // this.app.logger.info('Mongo Status:'+ mongoose.connection.readyState);

  }

  public async boot() {
    // All bindings are ready, feel free to use them
  }

  public async ready() {
    // this.app.logger.info('From Mongoose Provider')
  }

  public async shutdown() {
    await mongoose.disconnect();
    // Cleanup, since app is going down
  }
}

import type { ApplicationContract } from '@ioc:Adonis/Core/Application'
// import UserService from 'App/Service/UserService'

export default class AppProvider {
  constructor (protected app: ApplicationContract) {
  }

  public register () {
    // Register your own bindings

    // this.app.container.singleton('UserService',() => new UserService());
  }

  public async boot () {
    // IoC container is ready
  }

  public async ready () {
    // App is ready
  }

  public async shutdown () {
    // Cleanup, since app is going down
  }
}

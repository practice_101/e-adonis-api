import { Application, BaseCommand, Kernel } from '@adonisjs/core/build/standalone'
import BaseSeeder from 'Database/Seeders/BaseSeeder'
import { inject } from '@adonisjs/fold';

@inject()
export default class DbSeedCommand extends BaseCommand {
  /**
   * Command name is used to run the command
   */
  public static commandName = 'db:seed'

  /**
   * Command description is displayed in the "help" output
   */
  public static description = 'Run database seeders'

  public static settings = {
    /**
     * Set the following value to true, if you want to load the application
     * before running the command. Don't forget to call `node ace generate:manifest`
     * afterwards.
     */
    loadApp: true,

    /**
     * Set the following value to true, if you want this command to keep running until
     * you manually decide to exit the process. Don't forget to call
     * `node ace generate:manifest` afterwards.
     */
    stayAlive: false,
  }
  public static providers = ['providers/MongooseProvider'];
  private seeder : BaseSeeder

  constructor(application : Application, kernel : Kernel, seeder : BaseSeeder){
    super(application,kernel)
    this.seeder = seeder;
  }

  public static start() {
    return { loadApp: true };
  }

  public async run() {
    try {
      await this.seeder.run();
      this.logger.success('Database seeding completed');
    } catch (error) {
      this.logger.error(error.message);
    }

  }
}

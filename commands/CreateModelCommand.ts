import { BaseCommand, args } from '@adonisjs/core/build/standalone';
import { join } from 'path';

export default class CreateModelCommand extends BaseCommand {
  /**
   * Command name is used to run the command
   */
  public static commandName = 'create:model'
  @args.string({ description: 'Name of the model' })
  public name: string

  /**
   * Command description is displayed in the "help" output
   */
  public static description = 'Make a new Model'

  public static settings = {
    /**
     * Set the following value to true, if you want to load the application
     * before running the command. Don't forget to call `node ace generate:manifest`
     * afterwards.
     */
    loadApp: false,

    /**
     * Set the following value to true, if you want this command to keep running until
     * you manually decide to exit the process. Don't forget to call
     * `node ace generate:manifest` afterwards.
     */
    stayAlive: false,
  }

  public async run() {
    const name = this.name;
    this.generator
      .addFile(name)
      .appRoot(this.application.appRoot)
      .destinationDir('app/Models')
      .useMustache()
      .stub(join(__dirname, './templates/model.txt'))
      .apply({ name })

    await this.generator.run()

  }
}

import { Ioc } from '@adonisjs/fold'
import HashService from 'App/Service/HashService';
import UserService from 'App/Service/UserService'
import BaseSeeder from 'Database/Seeders/BaseSeeder';


Ioc.bind('UserService',() => new UserService());
Ioc.bind('BaseSeeder',() => new BaseSeeder());
Ioc.bind('HashService',() => new HashService());


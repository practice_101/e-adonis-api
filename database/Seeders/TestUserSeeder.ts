import UserModel from "App/Models/User";


export default class TestUserSeeder {
  public static async run(){
    const exists = await UserModel.exists({email:'jack@test.com'});
    if(!exists){
      await UserModel.create({
        name:'jack',
        email:'jack@test.com',
        password:'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'
      })
    }
  }
}



import TestUserSeeder from "./TestUserSeeder";

export default class BaseSeeder {
  public async run(){
    await Promise.all([
      TestUserSeeder.run()
    ])
  }
}


